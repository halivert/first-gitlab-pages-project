import { createApp, ref } from "vue";

createApp({
	setup(props) {
		const count = ref(0);

		const sum = () => {
			count.value++;
		};

		return {
			count,
			sum,
		};
	},
}).mount("#main");
