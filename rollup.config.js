import { defineConfig } from "rollup";
import nodeResolve from "@rollup/plugin-node-resolve";
import replace from "@rollup/plugin-replace";
import alias from "@rollup/plugin-alias";
import vue from "rollup-plugin-vue";

export default (args) => {
	return defineConfig([
		{
			input: "src/js/main.js",
			output: {
				dir: "assets/js",
				format: "esm",
				compact: true,
				manualChunks: {
					vue: ["vue"],
				},
			},
			plugins: [
				vue(),
				alias({
					entries: {
						vue: require.resolve("vue/dist/vue.esm-bundler.js"),
					},
				}),
				nodeResolve(),
				replace({
					values: {
						"process.env.NODE_ENV": JSON.stringify(
							args.configProd ? "production" : "development"
						),
						__VUE_OPTIONS_API__: true,
						__VUE_PROD_DEVTOOLS__: false,
					},
					preventAssignment: true,
				}),
			],
		},
	]);
};
